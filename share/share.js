var self_url = location.href.split('#')[0];
console.log('self_url:'+self_url);
var self_url_encode = encodeURIComponent(self_url);
var sign_api = 'http://www.qmcsl.cn/wechat/share/share.php?self_url='+self_url_encode;

var pack = null;

Zepto.getJSON(sign_api, function (sign_pack) {
    console.log('sign_pack:'+JSON.stringify(sign_pack))
    pack = sign_pack;
    wx.config({
        debug: false,
        appId: pack.appId,
        timestamp: pack.timestamp,
        nonceStr: pack.nonceStr,
        signature: pack.signature,
        jsApiList: [
            'onMenuShareTimeline',
            'onMenuShareAppMessage',
		    'checkJsApi',
            'startRecord',
            'stopRecord',
            'translateVoice',
	        'playVoice',
		    'uploadVoice',
	        'downloadVoice'
        ]
    });

    wx.error(function(res){
    	console.log('微信分享权限注入失败！'+res.errMsg);
    });

    wx.ready(function(){
      sharePage();
    });
});

function sharePage() {
    var redpacket_conf = {
        title:pack.gametitle,
        desc:pack.gamedesc,
        link:pack.gamelink,
        imgUrl:pack.gameIconUrl
    }

    wx.onMenuShareAppMessage(redpacket_conf);
    wx.onMenuShareTimeline(redpacket_conf);
}

//红包分享
function shareRedpacket(code) {
    var redpacket_conf = {
        title:pack.gametitle,
        desc:"我给你发了一个红包！兑换码是"+code,
        link:pack.gamelink+"?&rpcode="+code,
        imgUrl:pack.gameIconUrl
    }

    wx.onMenuShareAppMessage(redpacket_conf);
    wx.onMenuShareTimeline(redpacket_conf);
}

//邀请好友注册
function shareInvite(data) {
    var invite_conf = {
        title:data.title?data.title:pack.gametitle,
        desc:data.desc?data.desc:pack.gamedesc,
        link:data.link?data.link:pack.gamelink,
        imgUrl:data.imgUrl?data.imgUrl:pack.gameIconUrl
    }

    wx.onMenuShareTimeline(invite_conf);
    wx.onMenuShareAppMessage(invite_conf);
}

//邀请进入房间
function shareGameRoom(data) {
    var roominvite_conf = {
        title:data.title?data.title:pack.gametitle,
        desc:"速来应战！房间号["+data.roomid+"]",
        link:pack.gamelink+"?&roomid="+data.roomid,
        imgUrl:data.imgUrl?data.imgUrl:pack.gameIconUrl
    }

    wx.onMenuShareTimeline(roominvite_conf);
    wx.onMenuShareAppMessage(roominvite_conf);
}

//总结算面板分享
function shareClearing(clear_id) {
    var clearing_conf = {
        title:pack.gametitle,
        desc:"对战结算在这里查看。",
        link:pack.gamelink+"?&clearingshare="+clear_id,
        imgUrl:pack.gameIconUrl
    }

    wx.onMenuShareTimeline(clearing_conf);
    wx.onMenuShareAppMessage(clearing_conf);
}
















